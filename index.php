<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birth_date'] = !empty($_COOKIE['birth_date_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    $errors['ability'] = !empty($_COOKIE['ability_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div>Поле имя не заполнено или введены недопустимые символы.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div>Поле почта не заполнено или введены недопустимые символы.</div>';
    }
    if ($errors['birth_date']) {
        setcookie('birth_date_error', '', 100000);
        $messages[] = '<div>Заполните дату рождения.</div>';
    }
    if ($errors['gender']) {
        setcookie('gender_error', '', 100000);
        $messages[] = '<div>Выберите пол.</div>';
    }
    if ($errors['limb']) {
        setcookie('limb_error', '', 100000);
        $messages[] = '<div>Выберие число конечностей.</div>';
    }
    if ($errors['ability']) {
        setcookie('ability_error', '', 100000);
        $messages[] = '<div>Выберие сверхспособности.</div>';
    }
    if ($errors['biography']) {
        setcookie('biography_error', '', 100000);
        $messages[] = '<div>Заполните биографию.</div>';
    }
    if ($errors['checkbox']) {
        setcookie('checkbox_error', '', 100000);
        $messages[] = '<div>Подтвердите, что ознакомились с контрактом.</div>';
    }
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['birth_date'] = empty($_COOKIE['birth_date_value']) ? '' : $_COOKIE['birth_date_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
    $values['ability'] = empty($_COOKIE['ability_value']) ? '' : $_COOKIE['ability_value'];
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : $_COOKIE['checkbox_value'];
    include('form.php');
}

else {
    $errors = FALSE;
    if (!preg_match("/^[A-Za-z]+$/", $_POST['fio']) || empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }

    if (!preg_match("/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/", $_POST['email']) || empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['birth_date']) || !is_numeric($_POST['birth_date'])) {
        setcookie('birth_date_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('birth_date_value', $_POST['birth_date'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['gender'])) {
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['limb']) || !is_numeric($_POST['limb'])) {
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
    }

    $myselect = $_POST['select'];
    $flag = 0;
    if (empty($myselect)) {
        setcookie('ability_error', '1', time() + 24 * 60 * 60);
        $flag = 1;
        $errors = TRUE;
    }
    if (!empty($myselect)) {
        foreach ($myselect as $ability) {
            if (!is_numeric($ability)) {
                setcookie('ability_error', '2', time() + 24 * 60 * 60);
                $flag = 1;
                $errors = TRUE;
                break;
            }
        }
    }
    if ($flag == 0) {
        setcookie('ability_error', $_POST['ability'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['biography'])) {
        setcookie('biography_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['checkbox']) || !is_numeric($_POST['checkbox'])) {
        setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('checkbox_error', '', 100000);
    }

    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('birth_date_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('limb_error', '', 100000);
        setcookie('ability_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('checkbox_error', '', 100000);
    }

    $user = 'u20407';
    $pass = '8201377';
    $db = new PDO('mysql:host=localhost;dbname=u20407', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $stmt = $db->prepare("INSERT INTO form (name, email, year, gender, limb, bio, checkbox) VALUES (:fio, :email, :birth_date, :gender, :limb, :bio, :checkbox)");
    $stmt -> execute(array('fio'=>$_POST['fio'], 'email'=>$_POST['email'], 'birth_date'=>$_POST['birth_date'],'gender'=>$_POST['gender'],'limb'=>$_POST['limb'],'bio'=>$_POST['biography'],'checkbox'=>$_POST['checkbox']));

    $form_id =  $db->lastInsertId();
    $myselect = $_POST['select'];
    if (!empty($myselect)) {
        foreach ($myselect as $ability) {
            if (!is_numeric($ability)) {
                continue;
            }
            $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:form_id, :ability_id)");
            $stmt -> execute(array(
                'form_id' => $form_id,
                'ability_id' => $ability
            ));
        }
    }
    setcookie('save', '1');
    header('Location: index.php');
}